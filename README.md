# RTL HowToExperiment

## RTL Lab usage project

Usage (for RTL lab members)
The document that creates the book is an R Quarto book.
Please use git workflow to update the document.

git pull

change files ...
render the book anew in RStudio

git commit -a 
git push

The site should then run the CI and show the book online under: 
https://ccns.gitlab.io/reachandtouch/rtl-howtoexperiment/

Note: the CI does only copy the content from _book to public
